#  Node.js + Open Telemetry + Prometheus

*  Open Telemetry の概要については <https://syu-m-5151.hatenablog.com/entry/2022/07/12/115434> がわかりやすい.
    -  現在勢いがあるオブザーバビリティ (メトリクス、トレース、ログ) の計装と収集の標準化プロジェクト.
    -  まだまだ新しいため、言語ごとのサポートはまちまち. (特にログはすべて Experimental 以下)
        +  <https://opentelemetry.io/docs/instrumentation/>

*  関連ツールのトレンド
    -  Prometheus + Grafana が強い. OpenTelemetry は今後伸びてきそう.
    -  <https://trends.google.com/trends/explore?date=all&q=%2Fg%2F11h0b6gwth,Jaegar,Zipkin,OpenTelemetry,Grafana&hl=en>
    -  <https://codersociety.com/blog/articles/jaeger-vs-zipkin-vs-tempo>

*  Grafana は OSS のダッシュボードツールであり、様々な統計情報を一画面で可視化できる.
    -  Grafana は様々なデータ取得元がサポートされており、MySql、PostgreSQL、Zabbix、Prometheus、Elasticsearch などが利用できる.
    -  データ取得元として Prometheus を活用することで、Prometheus が集めてきたメトリクス情報を Grafana で可視化できる.
    -  一応 Prometheus 自体も可視化のための UI を持ってはいるが、Grafana のほうがリッチであるようだ.
    -  <https://www.designet.co.jp/faq/term/?id=R3JhZmFuYQ>
    -  <https://grafana.com/oss/prometheus/>

*  OpenTelemetry のドキュメントを見る限り、ツールによってサポートされているオブザーバビリティが異なっている模様.
    -  Prometheus はメトリクス情報用.
        +  <https://opentelemetry.io/docs/specs/otel/metrics/sdk_exporters/>
    -  Jaegar や Zipkin はトレース情報用.
        +  <https://opentelemetry.io/docs/specs/otel/trace/>

*  最近 Grafana が Jaegar や Zipkin もサポートしたようなので、Prometheus / Jaegar / Zipkin のすべての情報は Grafana で可視化できるらしい？

*  サーバ監視を行ってくれる外部の Saas サービス (いずれも基本的には有料)
    -  Mackerel
    -  Datadog
    -  New Relic

##  サンプルアプリケーションの用意

*  `npm init -y`
*  `npm install express`
*  `vim app.js`

```js
'use strict';

const express = require('express');
const app = express();
const PORT = process.env.PORT || '8000';

app.use((req, res, next) => {
  // ランダムに色を選ぶ
  const color = ['red', 'blue', 'blue', 'yellow', 'yellow', 'yellow'];
  res.locals.color = color[Math.floor(Math.random() * color.length)];
  next();
});

app.get('/', (req, res, next) => {
  res.send(res.locals.color);
  next();
});

app.listen(parseInt(PORT, 10), () => {
  console.log(`Listening for requests on http://localhost:${PORT}`);
});
```

*  サンプルの実行.
    -  `node app.js`
    -  `curl localhost:8000`
        +  `red` or `blue` or `yellow` と表示される.

##  Prometheus の起動

*  メトリクス収集の準備として、メトリクスデータのグラフ化を行う Prometheus を用意する.

*  `vim prometheus.yml`
    -  `targets` は Prometheus におけるメトリクスデータの収集先.
    -  今回はホストとなるが、コンテナから見たホストは `localhost` ではなく `host.docker.internal` となる.

```yml
global:
  scrape_interval: 15s

scrape_configs:
  - job_name: 'opentelemetry'
    static_configs:
      - targets:
        - 'host.docker.internal:9464'
```

*  `vim compose.yml`

```yml
services:
  prometheus:
    image: prom/prometheus:v2.44.0
    ports:
      - 9090:9090
    volumes:
      - ./prometheus.yml:/etc/prometheus/prometheus.yml
```

*  `docker compose up -d`

*  ブラウザで <http://localhost:9090> にアクセスすると Prometheus の GUI が確認できる.

##  サンプルアプリケーションにメトリクスデータ収集を行う Open Telemetry を組み込む

*  インストール
    -  `npm install @opentelemetry/sdk-metrics @opentelemetry/exporter-prometheus`

*  `vim monitoring.js`
    -  サンプルアプリケーションに組み込むカウンタのロジックを作成.
    -  `meter.createCounter()` でカウンタが作成される. 引数で指定した文字列がメトリクスデータのテーブル名に相当する.

```js
'use strict';

const { MeterProvider } = require('@opentelemetry/sdk-metrics');
const { PrometheusExporter } = require('@opentelemetry/exporter-prometheus');

const prometheusPort = PrometheusExporter.DEFAULT_OPTIONS.port;
const prometheusEndpoint = PrometheusExporter.DEFAULT_OPTIONS.endpoint;
const prometheusExporter = new PrometheusExporter(
  { startServer: true },
  () => {
    console.log(`prometheus scrape endpoint: http://localhost:${prometheusPort}${prometheusEndpoint}`);
  }
);

const meterProvider = new MeterProvider();
meterProvider.addMetricReader(prometheusExporter);
const meter = meterProvider.getMeter('color-meter');
const colorCounter = meter.createCounter('colors', {
  description: 'Count each color'
});

module.exports.countColorRequests = () => {
  return (req, res, next) => {
    switch (res.locals.color) {
      case 'red':
        colorCounter.add(1, { color: 'red' });
        break;
      case 'blue':
        colorCounter.add(1, { color: 'blue' });
        break;
      case 'yellow':
        colorCounter.add(1, { color: 'yellow' });
        break;
    }
    next();
  };
};
```

*  `vim app.js`
    -  `monitoring.js` をサンプルに組み込む.
    -  Express.js の Application-level middleware として `monitoring.js` 中の `countColorRequests` 関数を登録する.
    -  これにより、先行する middleware で計算した結果を取得し、カウンタに反映させる.
    -  <https://expressjs.com/en/guide/using-middleware.html>

```js
'use strict';

const express = require('express');
const app = express();
const PORT = process.env.PORT || '8000';

const { countColorRequests } = require('./monitoring');

app.use((req, res, next) => {
  // ランダムに色を選ぶ
  const color = ['red', 'blue', 'blue', 'yellow', 'yellow', 'yellow'];
  res.locals.color = color[Math.floor(Math.random() * color.length)];
  next();
});

app.use(countColorRequests());

app.get('/', (req, res, next) => {
  res.send(res.locals.color);
  next();
});

app.listen(parseInt(PORT, 10), () => {
  console.log(`Listening for requests on http://localhost:${PORT}`);
});
```

*  アプリケーションを起動する.
    -  `node app.js`
    -  すると `PrometheusExporter` に設定した出力も一緒に表示される.

```bash
$ node app.js
Listening for requests on http://localhost:8000
prometheus scrape endpoint: http://localhost:9464
```

*  何度か API を叩いて動作確認してみる.

```bash
for var in `seq 100`; do curl localhost:8000; done
```

```
blueblueredblueyellowredblueyellowbluebluebluered
redyellowredyellowblueyellowyellowyellowyellowblu
yellowblueredyellowredblueyellowblueblueyellowblu
eyellowredblueblueyellowblueblueyellowyellowyello
blueyellowyellowredyellowyellowyellowyellowyellow
```

*  メトリクスデータを取得してみる.
    -  Prometheus がこの内容を定期的に収集してくれるようになる.

```bash
$ curl http://localhost:9464/metrics             
# HELP target_info Target metadata
# TYPE target_info gauge
target_info{service_name="unknown_service:node",t
pentelemetry",telemetry_sdk_version="1.13.0"} 1
# HELP colors_total Count each color
# TYPE colors_total counter
colors_total{color="blue"} 36 1684645640404
colors_total{color="red"} 22 1684645640404
colors_total{color="yellow"} 42 1684645640404
```

##  Prometheus でメトリクス情報を可視化してみる

*  ブラウザで <http://localhost:9090> にアクセスし、ナビゲーションバー中から `Graph` をクリック.
*  検索クエリとして `colors_total` と入力して `Execute` ボタンをクリックする.
*  すると先程作成した Node.js のアプリケーションからメトリクス情報が取得され画面に表示される.
*  `Graph` タブをクリックし、タイムレンジを適切に設定すると以下のようなグラフを表示できる.

![Prometheus](/images/prometheus_with_otel.png)

##  References

*  <https://qiita.com/raichi/items/d371bf3fe6ddec168725>
*  <https://www.npmjs.com/package/@opentelemetry/sdk-metrics>
*  <https://www.npmjs.com/package/@opentelemetry/exporter-prometheus>

