'use strict';

const { MeterProvider } = require('@opentelemetry/sdk-metrics');
const { PrometheusExporter } = require('@opentelemetry/exporter-prometheus');

const prometheusPort = PrometheusExporter.DEFAULT_OPTIONS.port;
const prometheusEndpoint = PrometheusExporter.DEFAULT_OPTIONS.endpoint;
const prometheusExporter = new PrometheusExporter(
  { startServer: true },
  () => {
    console.log(`prometheus scrape endpoint: http://localhost:${prometheusPort}${prometheusEndpoint}`);
  }
);

const meterProvider = new MeterProvider();
meterProvider.addMetricReader(prometheusExporter);
const meter = meterProvider.getMeter('color-meter');
const colorCounter = meter.createCounter('colors', {
  description: 'Count each color'
});

module.exports.countColorRequests = () => {
  return (req, res, next) => {
    switch (res.locals.color) {
      case 'red':
        colorCounter.add(1, { color: 'red' });
        break;
      case 'blue':
        colorCounter.add(1, { color: 'blue' });
        break;
      case 'yellow':
        colorCounter.add(1, { color: 'yellow' });
        break;
    }
    next();
  };
};

